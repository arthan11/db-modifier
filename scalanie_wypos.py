import os
import time
import sys
from shutil import copyfile
from core import DBModifier, Database, Table
from importer_wypos import ImporterWypos

class Importer(DBModifier, ImporterWypos):
    pass
    
if __name__ == "__main__":
    folder = os.path.dirname(os.path.abspath(__file__))
    folder = os.path.join(os.path.dirname(folder), 'scalenie', '')
    DB1 = r'FIN_20140909_143822.GBK.GDB'
    DB2 = r'FIN_20140911_092848.GBK.GDB'
    # reset baz danych
    try:
        if os.path.isfile(os.path.join(folder, DB1)):
            os.remove(os.path.join(folder, DB1))
        if os.path.isfile(os.path.join(folder, DB2)):
            os.remove(os.path.join(folder, DB2))
        # print os.path.join(folder, 'gbk', DB1)
        copyfile(os.path.join(folder, 'gbk', DB1), os.path.join(folder, DB1))
        copyfile(os.path.join(folder, 'gbk', DB2), os.path.join(folder, DB2))
    except:
        print 'Nie mozna zresetowac bazy danych!'
        sys.exit()

    start = time.time()
    imp = Importer()
    imp.AddDB(folder+DB1)
    imp.AddDB(folder+DB2)

    imp.mv_db1 = imp.databases[0]
    imp.mv_db2 = imp.databases[1]

    imp.clear_column(imp.mv_db1, 'WYPOS_TRANS', 'NUMER_INWENTARZOWY')
    imp.clear_column(imp.mv_db1, 'WYPOS_TRANS', 'NRINW')
    imp.clear_column(imp.mv_db1, 'WYPOS_TRANS', 'NRINW_PREFIX')
    imp.clear_column(imp.mv_db1, 'WYPOS_TRANS', 'NRINW_SUFFIX')
    imp.clear_column(imp.mv_db1, 'WYPOS_TRANS', 'NRINW_SORT')

    imp.clear_column(imp.mv_db1, 'SRODKI', 'NUMER_INWENTARZOWY')
    imp.clear_column(imp.mv_db1, 'SRODKI', 'NRINW')
    imp.clear_column(imp.mv_db1, 'SRODKI', 'NRINW_PREFIX')
    imp.clear_column(imp.mv_db1, 'SRODKI', 'NRINW_SUFFIX')
    imp.clear_column(imp.mv_db1, 'SRODKI', 'NRINW_SORT')

    imp.move_JEDNOSTKI([10])
    imp.move_ODPOWIEDZIALNI()
    imp.move_UMIEJSCOWIENIE()
    imp.move_SRODKI()
    imp.move_AMORTYZACJA()
    imp.move_GRUPY_WYPOSAZENIA()
    imp.move_WYPOS_TRANS()
    imp.move_WYPOS_KOMISJE()
    imp.move_INWENTARYZACJA()
    imp.move_INWENT_ARKUSZE()
    imp.move_INWENT_LISTA()
    imp.move_ZWIEKSZ_WART()
    imp.change_PK(imp.mv_db2, 'JEDNOSTKI', 17, 6)
    imp.zlacz_duble('ODPOWIEDZIALNI', ['NAZWISKO', 'IMIE', 'ID_JEDN', 'ID_GR'])
    imp.zlacz_duble('UMIEJSCOWIENIE', ['NAZWA', 'ID_JEDN', 'ID_GR'])
    imp.zlacz_duble('GRUPY_WYPOSAZENIA', ['NAZWA', 'TYP', 'ID_JEDN', 'ID_GR'])
    imp.delete(imp.mv_db2, 'JEDNOSTKI', 17)


    end = time.time()
    print 'Ukonczono w %.2fs.' % (end - start)