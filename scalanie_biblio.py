import os
import time
import sys
from shutil import copyfile
from core import DBModifier, Database, Table
from importer_biblio import ImporterBiblio
from db_info import DBInfo

class Importer(DBModifier, ImporterBiblio):
    pass

    
if __name__ == "__main__":
    folder = os.path.dirname(os.path.abspath(__file__))
    folder = os.path.join(folder, 'biblio_scalanie', '')
    DB1 = r'BIBLIO1.GDB'
    DB2 = r'biblio2.gdb'
    # reset baz danych
    try:
        if os.path.isfile(os.path.join(folder, DB1)):
            os.remove(os.path.join(folder, DB1))
        if os.path.isfile(os.path.join(folder, DB2)):
            os.remove(os.path.join(folder, DB2))
        #print os.path.join(folder, 'db', DB1), os.path.join(folder, DB1)
        copyfile(os.path.join(folder, 'db', DB1), os.path.join(folder, DB1))
        copyfile(os.path.join(folder, 'db', DB2), os.path.join(folder, DB2))
    except:
        print 'Nie mozna zresetowac bazy danych!'
        sys.exit()

    start = time.time()
    imp = Importer()
    imp.AddDB(folder+DB1)
    imp.AddDB(folder+DB2)

    imp.mv_db1 = imp.databases[0]
    imp.mv_db2 = imp.databases[1]

    info = DBInfo()
    info.host = '127.0.0.1'
    for dbname in [folder+DB1, folder+DB2]:
        info.database = dbname
        info.LoadDB()
        l = info.get_programs_versions()
        for p in l:
            if p['app'] == 'BIBLIOTEKA':
                print '{} - {} ver.{}'.format(os.path.basename(dbname), p['app'], p['ver'])
                break

    imp.move_TYP_JEDNOSTKI()
    imp.move_JEDNOSTKI()#[10])
    imp.move_ROK_SZKOLNY()
    imp.move_JEDN_KLASY()
    imp.move_CZYT_TYP()
    imp.move_CZYTELNIK()
    imp.move_CZYT_KLASA()
    imp.move_TYP_KUPNA()
    
    imp.mv_db2.cur.execute('ALTER SEQUENCE %s RESTART WITH %d' % ('K_TYP_DOK_GEN', 7))
    imp.mv_db2.cur.execute('ALTER SEQUENCE %s RESTART WITH %d' % ('K_TYP_DOK_GEN', 7))
    
    imp.move_K_TYP_DOK()
    imp.move_K_OPIS()
    imp.move_DOSTEPNOSC()
    imp.move_TYP_WPLYWU()
    imp.move_TYP_UBYTKU()
    imp.move_KSIEGA_INW()
    imp.move_INWENTARZ()
    imp.move_WYPOZYCZENIA()
    imp.move_CZYTELNIA()
    #imp.move_SPIS_Z_NATURY()
    imp.move_K_INSTYT()
    imp.move_K_ISBN()
    imp.move_TYP_KSIAZKI()
    imp.move_K_TYTUL_DOD()
    imp.move_TYP_ODP()
    imp.move_K_ODPOW()
    #imp.move_K_OPIS_DET()
    imp.move_K_WYDAWCA()
    imp.move_K_HASLO()
    imp.move_K_HASLO_OPISU()
    #imp.move_K_TYP_UWAGI()
    #imp.move_K_UWAGA()
    imp.move_K_SERIA()
    imp.move_K_SERIA_OPISU()
    imp.move_LEGITYMACJE()
    imp.move_K_JEZYK_OPISU()
    imp.move_ZDJECIE()
    imp.move_K_ZAGADN_TYP_DOK()
    imp.move_POWIADOMIENIA()
    imp.move_ZEST_BIBLIOGRAFICZNE()
    imp.move_K_ZAGADNIENIOWA_NEW()
    imp.move_ZEST_BIBLIOGRAF_POZYCJE()
    imp.move_K_ZAGADN_HASLO()
    imp.move_CZYTELNIA_ZBIORY()
    imp.move_CZYT_IMIONA()
    #imp.move_CZYT_MIASTA()
    #imp.move_MARC_LANG()
    imp.move_ZAMOWIENIA()
    imp.move_PROTOKOLY_UBYTKOW()
    imp.move_REJESTR_UBYTKOW()
    imp.move_MONITY()
    imp.move_MONITY_POZYCJE()
    imp.move_MONITY_WYSYLKA()
    #imp.move_MONITY_OPISY()
    imp.move_PRACOWNIA()
    imp.move_AC_RODZAJE()
    imp.move_AC_CZASOPISMA()
    imp.move_AC_NUMERY()
    imp.move_CZYTELNIA_AC()
    imp.move_KODY_ETYKIETY()
    imp.move_REJESTR_SKONTRUM()
    #imp.move_AUDIT_HEADER()
    #imp.move_AUDIT_DETAIL()
    #imp.move_AUDIT_IMAGE()
    imp.move_K_ZAGADN_NAST_OZN_ODP()
    imp.move_K_ZAGADN_UWAGA()
    imp.move_K_ZAGADN_INW()

    imp.zlacz_duble('K_TYP_DOK', ['NAZWA'])
    
    '''
    imp.zlacz_duble('STOL_GMPGHP', ['NAZWA'])
    
    
    imp.change_PK(imp.mv_db2, 'JEDNOSTKI', 18, 3)
    imp.delete(imp.mv_db2, 'JEDNOSTKI', 18)
    '''
    
    end = time.time()
    print 'Ukonczono w %.2fs.' % (end - start)