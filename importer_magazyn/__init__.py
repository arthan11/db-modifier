import os
import sys
import time
from shutil import copyfile
from kinterbasdb import connect, DESCRIPTION_NAME
from core import DBModifier

class ImporterMagazyn():
    def move_JEDNOSTKI(self, ids=[]):
        self.InitMoveData('JEDNOSTKI',
                          'ID_JEDN',
                          'NUM_JEDN_GEN',
                          ids)
        self.MoveData_MV()

    def move_TYPY_SZKOL(self):
        self.InitMoveData('TYPY_SZKOL',
                          'ID_TYP_S',
                          'GEN_TYPY_SZKOL')
        self.MoveData_MV()

    def move_KONTRAH(self):
        self.InitMoveData('KONTRAH',
                          'ID_KONTRAH',
                          'NUM_KONTRAH')
        self.AddFK_MV('TYPY_SZKOL', 'ID_TYP_S')
        self.MoveData_MV()

    def move_MAG_MAGAZYN(self):
        self.InitMoveData('MAG_MAGAZYN',
                          'IDMAG',
                          'GEN_MAG_MAGAZYN_ID')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()

    def move_MAG_DOKUMENTY(self):
        self.InitMoveData('MAG_DOKUMENTY',
                          'IDDOK',
                          'GEN_MAG_DOKUMENTY_ID')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        #self.AddFK_MV('KONT', 'ID_KONT')
        #self.AddFK_MV('ZESTAW_KONT', 'ID_ZES')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG_MM')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        #self.AddFK_MV('MAG_RODDOK', 'IDRODDOK') # poki co takie same
        self.AddFK_MV('MAG_DOKUMENTY', 'IDDOK_MM')
        self.AddFK_MV('KONTRAH', 'ID_KONTRAH')
        self.MoveData_MV()

    def move_MAG_GRUPY_TOW(self):
        self.InitMoveData('MAG_GRUPY_TOW',
                          'IDGRUPY',
                          'GEN_MAG_GRUPY_TOW_ID')
        self.MoveData_MV()

    def move_MAG_GRUPY_TOW_MAG(self):
        self.InitMoveData('MAG_GRUPY_TOW_MAG',
                          '',
                          '')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        self.AddFK_MV('MAG_GRUPY_TOW', 'IDGRUPY')
        self.MoveData_MV()
        
    def move_MAG_TOWARY(self):
        self.InitMoveData('MAG_TOWARY',
                          'ID_TOW',
                          'NUM_REJ_TOW')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        self.AddFK_MV('MAG_GRUPY_TOW', 'IDGRUPY')
        self.MoveData_MV()

    def move_MAG_REJESTR_TOWAR_ROK(self):
        self.InitMoveData('MAG_REJESTR_TOWAR_ROK',
                          '',
                          '')
        self.AddFK_MV('MAG_TOWARY', 'ID_TOW')
        self.MoveData_MV()

    def move_MAG_OPERACJE(self):
        self.InitMoveData('MAG_OPERACJE',
                          'IDOPE',
                          'GEN_MAG_OPERACJE_ID')
        self.AddFK_MV('MAG_OPERACJE', 'IDOPE_PRZYCH')
        #self.AddFK_MV('KONT', 'ID_KONT')
        #self.AddFK_MV('ZESTAW_KONT', 'ID_ZES')
        self.AddFK_MV('MAG_DOKUMENTY', 'IDDOK')
        self.AddFK_MV('MAG_TOWARY', 'ID_REJESTR_POZ')
        self.MoveData_MV()

    def move_MAG_DOKUMENTY(self):
        self.InitMoveData('MAG_DOKUMENTY',
                          'IDDOK',
                          'GEN_MAG_DOKUMENTY_ID')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        #self.AddFK_MV('KONT', 'ID_KONT')
        #self.AddFK_MV('ZESTAW_KONT', 'ID_ZES')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG_MM')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        #self.AddFK_MV('MAG_RODDOK', 'IDRODDOK') # poki co takie same
        self.AddFK_MV('MAG_DOKUMENTY', 'IDDOK_MM')
        self.AddFK_MV('KONTRAH', 'ID_KONTRAH')
        self.MoveData_MV()

    def move_STOL_ZAPOTRZEBOWANIE(self):
        self.InitMoveData('STOL_ZAPOTRZEBOWANIE',
                          'ID_ZAPOTRZEBOWANIE',
                          'GEN_STOL_ZAPOTRZEBOWANIE')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.AddFK_MV('MAG_DOKUMENTY', 'IDDOK')
        #self.AddFK_MV('STOL_TYP_GR_POSILKOWA', 'ID_TYP_GR_POS') # poki co puste
        self.AddFK_MV('KONTRAH', 'ID_KONTRAH')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        self.MoveData_MV()

    def move_STOL_GMPGHP(self):
        self.InitMoveData('STOL_GMPGHP',
                          'ID_GMPGHP',
                          'GEN_STOL_GMPGHP')
        self.MoveData_MV()

    def move_STOL_GMPGHP_REC(self):
        self.InitMoveData('STOL_GMPGHP_REC',
                          '',
                          'GEN_STOL_GMPGHP_REC')
        self.AddFK_MV('STOL_ZAPOTRZEBOWANIE', 'ID_ZAPOTRZEBOWANIE')
        self.AddFK_MV('STOL_GMPGHP', 'ID_GMPGHP')
        self.MoveData_MV()

    def move_STOL_GMPGHP_ZAPOT(self):
        self.InitMoveData('STOL_GMPGHP_ZAPOT',
                          '',
                          '')
        self.AddFK_MV('STOL_ZAPOTRZEBOWANIE', 'ID_ZAPOTRZEBOWANIE')
        self.AddFK_MV('STOL_GMPGHP', 'ID_GMPGHP')
        self.MoveData_MV()

    def move_STOL_GRUPY_OSOB(self):
        self.InitMoveData('STOL_GRUPY_OSOB',
                          'ID_GRUPY_OSOB',
                          'GEN_STOL_GRUPY_OSOB')
        self.AddFK_MV('STOL_ZAPOTRZEBOWANIE', 'ID_ZAPOTRZEBOWANIE')
        self.MoveData_MV()

    def move_STOL_POSILKI(self):
        self.InitMoveData('STOL_POSILKI',
                          'ID_POSILEK',
                          'GEN_STOL_POSILKI')
        self.AddFK_MV('STOL_ZAPOTRZEBOWANIE', 'ID_ZAPOTRZEBOWANIE')
        self.MoveData_MV()

    def move_STOL_SKLADNIKI(self):
        self.InitMoveData('STOL_SKLADNIKI',
                          'ID_SKLADNIK',
                          'GEN_STOL_SKLADNIKI')
        self.AddFK_MV('STOL_ZAPOTRZEBOWANIE', 'ID_ZAPOTRZEBOWANIE')
        self.AddFK_MV('STOL_POSILKI', 'ID_POSILEK')
        self.AddFK_MV('MAG_TOWARY', 'ID_TOW')
        self.MoveData_MV()
                
    def move_STOL_STAWKI_POSILKOW(self):
        self.InitMoveData('STOL_STAWKI_POSILKOW',
                          'ID_STOL_STAWKI_POSILKOW',
                          'GEN_STOL_STAWKI_POSILKOW')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        self.MoveData_MV()
                
    def move_STOL_WART_ODZYW(self):
        self.InitMoveData('STOL_WART_ODZYW',
                          '',
                          '')
        self.AddFK_MV('MAG_TOWARY', 'ID_TOW')
        self.MoveData_MV()

    def move_MAG_INWENT(self):
        self.InitMoveData('MAG_INWENT',
                          'ID_INWENT',
                          'GEN_MAG_INWENT_ID')
        self.AddFK_MV('MAG_MAGAZYN', 'IDMAG')
        self.MoveData_MV()      
        
    def move_MAG_INWENT_POZYCJE(self):
        self.InitMoveData('MAG_INWENT_POZYCJE',
                          'ID_INWENT_POZ',
                          'GEN_MAG_INWENT_POZYCJE_ID')
        self.AddFK_MV('MAG_TOWARY', 'ID_TOW')
        self.AddFK_MV('MAG_INWENT', 'ID_INWENT')
        self.MoveData_MV()      

    def move_MAG_KONTO_KSIEG(self):
        self.InitMoveData('MAG_KONTO_KSIEG',
                          '',
                          '')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()    

    def change_PK(self, db, table_name, old_id, new_id, in_root=False):
        sql_base = 'update %s set %s = %d where %s = %d'
        for tab in db.tables:
            for fk in db.tables[tab].fk:
                if fk[0] == table_name:
                    if True: #try:
                        self.alert_start('zmiana FK w tabeli %s z %d na %d' % (tab, old_id, new_id))
                        sql = sql_base % (tab,
                                          fk[1],
                                          new_id,
                                          fk[1],
                                          old_id)
                        db.cur.execute(sql)
                        field_idx = db.tables[tab].FieldIdx(fk[1])
                        if not field_idx:
                            self.alert_stop()
                            continue
                        for i, val in enumerate(db.tables[tab].values):
                            l = list(val)
                            if l[field_idx]:
                                l[field_idx] = new_id
                                self.databases[-1].tables[tab].values[i] = tuple(l)
                        self.alert_stop()
                    #except:
                    #    self.alert_stop_fail()

        if in_root:
            try:
                self.alert_start('zmiana PK w tabeli %s z %d na %d' % (table_name, old_id, new_id))
                pk_name = db.tables[table_name].pk_name
                sql = sql_base % (table_name,
                                  pk_name,
                                  new_id,
                                  pk_name,
                                  old_id)
                db.cur.execute(sql)
                self.alert_stop()
            except:
                self.alert_stop_fail()
        db.con.commit()

