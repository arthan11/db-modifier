﻿import os
import sys
import kinterbasdb

'''
lista obslugiwanych programow:
- Biblioteka
- Finanse
- Fundusz
- Kadry
- Kasa
- Kontrahenci
- Kontrola Budżetu
- KZP
- Magazyn/Stołówka
- Płace
- Poradnia
- Przelewy
- Rozrachunki/Fakturowanie/Rejestr VAT/Zamówienia Publiczne
- Wyposażenie
- Zlecone
'''

fin_apps_params = [['FINANSE', 5, 1015],
                   ['KASA', 8, 0],
                   ['KONTRAHENCI', 7, 0],
                   ['MAGAZYN', 1014, 0],
                   ['ROZRACHUNKI', 6, 0],
                   ['WYPOSAZENIE', 9, 0]
                  ]

kad_apps_params = [['KADRY', 30016, 30030],
                   ['PLACE', 30007, 30031],
                   ['ZLECONE', 1011, 30032],
                  ]

database = r'd:\temp_\db_modifier\info\finanse.gdb'
#database = r'd:\temp_\db_modifier\info\biblio.gdb'
#database = r'd:\temp_\db_modifier\info\kzp.gdb'
#database = r'd:\temp_\db_modifier\info\fundusz.gdb'
#database = r'd:\temp_\db_modifier\info\kadry.gdb'
#database = r'd:\temp_\db_modifier\info\ppp.gdb'
#database = r'd:\temp_\db_modifier\info\sko.gdb'
#database = r'd:\temp_\db_modifier\info\planowanie.gdb'


con = kinterbasdb.connect(host='127.0.0.1', 
              database=database,
              user='sysdba',
              password='masterkey')
cur = con.cursor()

def pad_left(txt, size, char=' '):
    return char*(size-len(txt)) + txt

def table_exists(table_name):
    cur.execute("select 1 from rdb$relations where rdb$relation_name = '%s';" % (table_name))
    ret = cur.fetchone()
    if ret:
        ret = ret[0]
    return (ret == 1)

def get_fin_param(param_id):
    cur.execute("select OPIS from PARAMETR where ID_PAR=%d" % (param_id))
    param = cur.fetchone()
    if param:
        param = param[0]
    return param

def get_biblio_param(param_id):
    cur.execute("select WARTOSC from PARAMETRY where ID=%d" % (param_id))
    param = cur.fetchone()
    if param:
        param = param[0]
    return param

def get_kad_param(param_id):
    param_id = pad_left(str(param_id), 10, '0')
    cur.execute("select OPIS from KAD_PARAMETR where NUMERID=%s" % (param_id))
    param = cur.fetchone()
    if param:
        param = param[0]
        if param == '':
            param = None
    return param

def get_prz_param(param_id):
    param_id = pad_left(str(param_id), 10, '0')
    cur.execute("select OPIS from PRZ_PARAMETR where NUMERID=%s" % (param_id))
    param = cur.fetchone()
    if param:
        param = param[0]
        if param == '':
            param = None
    return param

def get_program_list():
    cur.execute("select NAZWA from UR_PROGRAMS")
    ret = cur.fetchall()
    list = []
    for p in ret:
        list.append(p[0])
    return list
	
program_list = get_program_list()

# Finanse, Kasa, Kontrahenci, Magazyn, Rozrachunki, Wyposazenie
if 'FinanseDDJ' in program_list:
    jedn = get_fin_param(10)
    print 'JEDNOSTKA:', jedn
    
    #for app, par in fin_apps_params:
    #    print app+':', get_fin_param(par)
    for app, par_old, par_new in fin_apps_params:
        ver = get_fin_param(par_new)
        if not ver:
            ver = get_fin_param(par_old)
        print app+':', ver
    
# Biblioteka
if 'Biblioteka' in program_list:
    jedn = get_biblio_param(10)
    print 'JEDNOSTKA:', jedn
    print 'BIBLIOTEKA:', get_biblio_param(300)
    
# KZP
if 'KZP' in program_list:
    jedn = get_fin_param(10)
    ver = get_fin_param(1011)
    print 'JEDNOSTKA:', jedn
    print 'KZP:', ver

# Fundusz
if 'FUNDUSZ' in program_list:
    jedn = get_fin_param(10)
    ver = get_fin_param(1011)
    print 'JEDNOSTKA:', jedn
    print 'FUNDUSZ:', ver

# Kadry itp.
if 'Kadry_szkola' in program_list:
    jedn = get_kad_param(1)
    print 'JEDNOSTKA:', jedn
    for app, par_old, par_new in kad_apps_params:
        ver = get_kad_param(par_new)
        if not ver:
            ver = get_kad_param(par_old)
        print app+':', ver

# Przelewy
if 'Przelewy' in program_list:
	ver = get_prz_param(30013)
	if not ver:
		ver = get_prz_param(30009)
	print 'PRZELEWY:', ver
    
# PoradniaPP
if 'PoradniaPP' in program_list:
    jedn = get_fin_param(10)
    ver = get_fin_param(6)
    print 'JEDNOSTKA:', jedn
    print 'PORADNIA:', ver

# Kontrola Budżetu
if 'Kontrola Budżetu'.decode('utf-8') in program_list:
    jedn = get_fin_param(10)
    ver = get_fin_param(1001)
    print 'JEDNOSTKA:', jedn
    print 'Kontrola Budzetu:', ver
	
# Planowanie
if 'PLANOWANIE' in program_list:
    jedn = get_fin_param(10)
    ver = get_fin_param(1011)
    print 'JEDNOSTKA:', jedn
    print 'Planowanie:', ver
	
