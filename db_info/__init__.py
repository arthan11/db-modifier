# -*- coding: utf-8 -*-
from kinterbasdb import connect, DESCRIPTION_NAME

fin_apps_params = [['FINANSE', 5, 1015],
                   ['KASA', 8, 0],
                   ['KONTRAHENCI', 7, 0],
                   ['MAGAZYN', 1014, 0],
                   ['ROZRACHUNKI', 6, 0],
                   ['WYPOSAZENIE', 9, 0]
                  ]

kad_apps_params = [['KADRY', 30016, 30030],
                   ['PLACE', 30007, 30031],
                   ['ZLECONE', 1011, 30032],
                  ]

class DBInfo(object):
    def __init__(self):
        self.con = None
        self.cur = None
        self.host = ''
        self.port = '3050'
        self.database = ''
        self.user = 'sysdba'
        self.password = 'masterkey'

    def LoadDB(self):
        self.con = connect(dsn='{0}/{1}:{2}'.format(self.host, self.port, self.database),
                           user=str(self.user),
                           password=str(self.password))
        self.cur = self.con.cursor()
    
    def get_program_list(self):
        self.cur.execute("select NAZWA from UR_PROGRAMS")
        ret = self.cur.fetchall()
        list = []
        for p in ret:
            list.append(p[0])
        return list
        
    def pad_left(self, txt, size, char=' '):
        return char*(size-len(txt)) + txt

    def get_fin_param(self, param_id):
        self.cur.execute("select OPIS from PARAMETR where ID_PAR=%d" % (param_id))
        param = self.cur.fetchone()
        if param:
            param = param[0]
        return param

    def get_biblio_param(self, param_id):
        self.cur.execute("select WARTOSC from PARAMETRY where ID=%d" % (param_id))
        param = self.cur.fetchone()
        if param:
            param = param[0]
        return param

    def get_kad_param(self, param_id):
        param_id = self.pad_left(str(param_id), 10, '0')
        self.cur.execute("select OPIS from KAD_PARAMETR where NUMERID=%s" % (param_id))
        param = self.cur.fetchone()
        if param:
            param = param[0]
            if param == '':
                param = None
        return param

    def get_prz_param(self, param_id):
        param_id = self.pad_left(str(param_id), 10, '0')
        self.cur.execute("select OPIS from PRZ_PARAMETR where NUMERID=%s" % (param_id))
        param = self.cur.fetchone()
        if param:
            param = param[0]
            if param == '':
                param = None
        return param

    def get_programs_versions(self):
        ret = []
        program_list = self.get_program_list()
        
        # Finanse, Kasa, Kontrahenci, Magazyn, Rozrachunki, Wyposazenie
        if 'FinanseDDJ' in program_list:
            jedn = self.get_fin_param(10)
            
            #for app, par in fin_apps_params:
            #    print app+':', get_fin_param(par)
            for app, par_old, par_new in fin_apps_params:
                ver = self.get_fin_param(par_new)
                if not ver:
                    ver = self.get_fin_param(par_old)
                ret.append({'app': app, 'ver': ver})
            
        # Biblioteka
        if 'Biblioteka' in program_list:
            jedn = self.get_biblio_param(10)
            ret.append({'app': 'BIBLIOTEKA', 'ver': self.get_biblio_param(300)})
            
        # KZP
        if 'KZP' in program_list:
            jedn = self.get_fin_param(10)
            ver = self.get_fin_param(1011)
            #print 'JEDNOSTKA:', jedn
            ret.append({'app': 'KZP', 'ver': ver})

        # Fundusz
        if 'FUNDUSZ' in program_list:
            jedn = self.get_fin_param(10)
            ver = self.get_fin_param(1011)
            #print 'JEDNOSTKA:', jedn
            ret.append({'app': 'FUNDUSZ', 'ver': ver})

        # Kadry itp.
        if 'Kadry_szkola' in program_list:
            jedn = self.get_kad_param(1)
            #print 'JEDNOSTKA:', jedn
            for app, par_old, par_new in kad_apps_params:
                ver = self.get_kad_param(par_new)
                if not ver:
                    ver = self.get_kad_param(par_old)
                ret.append({'app': app, 'ver': ver})

        # Przelewy
        if 'Przelewy' in program_list:
            ver = self.get_prz_param(30013)
            if not ver:
                ver = self.get_prz_param(30009)
            ret.append({'app': 'PRZELEWY', 'ver': ver})
            
        # PoradniaPP
        if 'PoradniaPP' in program_list:
            jedn = self.get_fin_param(10)
            ver = self.get_fin_param(6)
            #print 'JEDNOSTKA:', jedn
            ret.append({'app': 'PORADNIA', 'ver': ver})

        # Kontrola Budżetu
        if 'Kontrola Budżetu'.decode('utf-8') in program_list:
            jedn = self.get_fin_param(10)
            ver = self.get_fin_param(1001)
            #print 'JEDNOSTKA:', jedn
            ret.append({'app': 'Kontrola Budżetu', 'ver': ver})
            
        # Planowanie
        if 'PLANOWANIE' in program_list:
            jedn = self.get_fin_param(10)
            ver = self.get_fin_param(1011)
            ret.append({'app': 'PLANOWANIE', 'ver': ver})
            
        return ret
        
    def get_jednostka(self, id_jedn):
        self.cur.execute("select * from JEDNOSTKI where ID_JEDN=%d" % (id_jedn))
        param = self.cur.fetchonemap()
        return param


if __name__ == '__main__':
    fb = DBInfo()
    fb.host = '127.0.0.1'
    fb.port = '3050'
    fb.database = r'd:\temp_\dj\django_db_test\FUNDUSZ.GDB'
    fb.user = 'sysdba'
    fb.password = 'masterkey'
    fb.LoadDB()
    
    #l = fb.get_program_list()
    l = fb.get_programs_versions()
    for p in l:
        print p