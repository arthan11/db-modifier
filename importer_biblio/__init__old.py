class ImporterBiblio(object):
    def move_TYP_JEDNOSTKI(self):
        self.InitMoveData('TYP_JEDNOSTKI',
                          'ID',
                          'GEN_TYP_JEDNOSTKI')
        self.MoveData_MV()

    def move_JEDNOSTKI(self, ids=[]):
        self.InitMoveData('JEDNOSTKI',
                          'ID_JEDN',
                          'GEN_JEDNOSTKI',
                          ids)
        self.AddFK_MV('TYP_JEDNOSTKI', 'ID_TYP_JEDNOSTKI')
        self.MoveData_MV()

    def move_JEDN_KLASY(self):
        self.InitMoveData('JEDN_KLASY',
                          'ID_KLASA',
                          'GEN_JEDN_KLASY')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()
        
    def move_CZYT_TYP(self):
        self.InitMoveData('CZYT_TYP',
                          'ID',
                          'GEN_CZYT_TYP')
        self.MoveData_MV()
        
    def move_CZYTELNIK(self):
        self.InitMoveData('CZYTELNIK',
                          'ID_CZYTELNIK',
                          'GEN_CZYTELNIK')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.AddFK_MV('CZYT_TYP', 'TYP')
        self.AddFK_MV('JEDN_KLASY', 'ID_KLASA')
        self.MoveData_MV()

    def move_K_TYP_DOK(self):
        self.InitMoveData('K_TYP_DOK',
                          'ID_TYP_DOK',
                          'K_TYP_DOK_GEN')
        self.MoveData_MV()
        
    def move_KSIEGA_INW(self):
        self.InitMoveData('KSIEGA_INW',
                          'ID_KS',
                          '')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        #self.AddFK_MV('K_TYP_DOK', 'DOM_ID_TYP_DOK')
        self.MoveData_MV()
        
    def move_TYP_KUPNA(self):
        self.InitMoveData('TYP_KUPNA',
                          'ID',
                          '')
        self.MoveData_MV()
        
    def move_TYP_UBYTKU(self):
        self.InitMoveData('TYP_UBYTKU',
                          'ID',
                          'GEN_TYP_UBYTKU')
        self.MoveData_MV()

    def move_TYP_WPLYWU(self):
        self.InitMoveData('TYP_WPLYWU',
                          'ID',
                          'GEN_TYP_WPLYWU')
        self.MoveData_MV()

    def move_K_OPIS(self):
        self.InitMoveData('K_OPIS',
                          'ID_KATALOG',
                          'GEN_K_OPIS')
        #self.AddFK_MV('K_TYP_DOK', 'ID_TYP_DOK')
        self.MoveData_MV()

    def move_DOSTEPNOSC(self):
        self.InitMoveData('DOSTEPNOSC',
                          'ID_DOSTEPNOSC',
                          '')
        self.MoveData_MV()
        
    def move_INWENTARZ(self):
        self.InitMoveData('INWENTARZ',
                          'ID_INW',
                          'GEN_INWENTARZ')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        #self.AddFK_MV('DOSTEPNOSC', 'ID_DOSTEPNOSC')
        #self.AddFK_MV('KSIEGA_INW', 'ID_KS')
        #self.AddFK_MV('TYP_UBYTKU', 'ID_TYP_UBT')
        #self.AddFK_MV('TYP_WPLYWU', 'ID_TYP_WPL')
        #self.AddFK_MV('TYP_KUPNA', 'ID_TYP_KUPNA')              
        self.MoveData_MV()  
    
    def move_AC_RODZAJE(self):
        self.InitMoveData('AC_RODZAJE',
                          'ID_RODZ',
                          '')
        self.MoveData_MV()
        
    def move_AC_CZASOPISMA(self):
        self.InitMoveData('AC_CZASOPISMA',
                          'ID_CZAS',
                          'GEN_ID_AC_CZASOPISMA')
        #self.AddFK_MV('AC_RODZAJE', 'ID_RODZ')
        self.MoveData_MV()
                        
    def move_AC_NUMERY(self):
        self.InitMoveData('AC_NUMERY',
                          'ID_NUM',
                          'GEN_ID_AC_NUMERY')
        self.AddFK_MV('AC_CZASOPISMA', 'ID_CZAS')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.MoveData_MV()
        
    def move_CZYTELNIA(self):
        self.InitMoveData('CZYTELNIA',
                          'ID_CZYTELNIK',
                          'GEN_CZYTELNIA')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.AddFK_MV('JEDN_KLASY', 'ID_KLASA')
        self.MoveData_MV()

    def move_CZYTELNIA_ZBIORY(self):
        self.InitMoveData('CZYTELNIA_ZBIORY',
                          '',
                          '')
        self.AddFK_MV('CZYTELNIA', 'ID_CZYTELNIA')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.MoveData_MV()
        
    def move_CZYTELNIA_AC(self):
        self.InitMoveData('CZYTELNIA_AC',
                          '',
                          '')
        self.AddFK_MV('CZYTELNIA', 'ID_CZYTELNIA')
        self.AddFK_MV('AC_NUMERY', 'ID_NUM')
        self.MoveData_MV()

    def move_CZYT_IMIONA(self):
        self.InitMoveData('CZYT_IMIONA',
                          'IMIE',
                          '')
        self.MoveData_MV()
