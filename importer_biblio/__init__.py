class ImporterBiblio(object):
    def move_TYP_JEDNOSTKI(self):
        self.InitMoveData('TYP_JEDNOSTKI',
                          'ID',
                          'GEN_TYP_JEDNOSTKI')
        self.MoveData_MV()

    def move_JEDNOSTKI(self):
        self.InitMoveData('JEDNOSTKI',
                          'ID_JEDN',
                          'GEN_JEDNOSTKI')
        self.AddFK_MV('TYP_JEDNOSTKI', 'ID_TYP_JEDNOSTKI')
        self.MoveData_MV()

    def move_ROK_SZKOLNY(self):
        self.InitMoveData('ROK_SZKOLNY',
                          'ID_ROK',
                          'GEN_ROK_SZKOLNY')
        self.MoveData_MV()

    def move_JEDN_KLASY(self):
        self.InitMoveData('JEDN_KLASY',
                          'ID_KLASA',
                          'GEN_JEDN_KLASY')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()

    def move_CZYT_TYP(self):
        self.InitMoveData('CZYT_TYP',
                          'ID',
                          'GEN_CZYT_TYP')
        self.MoveData_MV()

    def move_CZYTELNIK(self):
        self.InitMoveData('CZYTELNIK',
                          'ID_CZYTELNIK',
                          'GEN_CZYTELNIK')
        #self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        #self.AddFK_MV('CZYT_TYP', 'TYP')
        #self.AddFK_MV('JEDN_KLASY', 'ID_KLASA')
        self.MoveData_MV()

    def move_CZYT_KLASA(self):
        self.InitMoveData('CZYT_KLASA',
                          'ID',
                          'GEN_CZYT_KLASA')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.AddFK_MV('JEDN_KLASY', 'ID_KLASA')
        self.AddFK_MV('ROK_SZKOLNY', 'ID_ROK')
        self.MoveData_MV()

    def move_TYP_KUPNA(self):
        self.InitMoveData('TYP_KUPNA',
                          'ID',
                          '')
        self.MoveData_MV()

    def move_K_TYP_DOK(self):
        self.InitMoveData('K_TYP_DOK',
                          'ID_TYP_DOK',
                          'K_TYP_DOK_GEN')
        self.mv_db2.CorrectGenerator(self.mv_table_name)
        self.MoveData_MV()

    def move_K_OPIS(self):
        self.InitMoveData('K_OPIS',
                          'ID_KATALOG',
                          'GEN_K_OPIS')
        self.AddFK_MV('K_TYP_DOK', 'ID_TYP_DOK')
        self.MoveData_MV()

    def move_DOSTEPNOSC(self):
        self.InitMoveData('DOSTEPNOSC',
                          'ID_DOSTEPNOSC',
                          '')
        self.MoveData_MV()

    def move_TYP_WPLYWU(self):
        self.InitMoveData('TYP_WPLYWU',
                          'ID',
                          'GEN_TYP_WPLYWU')
        self.MoveData_MV()

    def move_TYP_UBYTKU(self):
        self.InitMoveData('TYP_UBYTKU',
                          'ID',
                          'GEN_TYP_UBYTKU')
        self.MoveData_MV()

    def move_KSIEGA_INW(self):
        self.InitMoveData('KSIEGA_INW',
                          'ID_KS',
                          '')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.AddFK_MV('K_TYP_DOK', 'DOM_ID_TYP_DOK')
        self.MoveData_MV()
        #print self.mv_db1, self.mv_db1

    def move_INWENTARZ(self):
        self.InitMoveData('INWENTARZ',
                          'ID_INW',
                          'GEN_INWENTARZ')
        self.AddFK_MV('TYP_KUPNA', 'ID_TYP_KUPNA')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.AddFK_MV('DOSTEPNOSC', 'ID_DOSTEPNOSC')
        self.AddFK_MV('TYP_WPLYWU', 'ID_TYP_WPL')
        self.AddFK_MV('TYP_UBYTKU', 'ID_TYP_UBT')
        #self.AddFK_MV('KSIEGA_INW', 'ID_KS')
        self.MoveData_MV()

    def move_WYPOZYCZENIA(self):
        self.InitMoveData('WYPOZYCZENIA',
                          'ID_WYP',
                          'GEN_WYPOZYCZENIA')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.MoveData_MV()

    def move_CZYTELNIA(self):
        self.InitMoveData('CZYTELNIA',
                          'ID_CZYTELNIA',
                          'GEN_CZYTELNIA')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.AddFK_MV('JEDN_KLASY', 'ID_KLASA')
        self.MoveData_MV()

    def move_SPIS_Z_NATURY(self):
        self.InitMoveData('SPIS_Z_NATURY',
                          '',
                          '')
        self.MoveData_MV()

    def move_K_INSTYT(self):
        self.InitMoveData('K_INSTYT',
                          'ID_INSTYT',
                          'GEN_K_INSTYT')
        self.MoveData_MV()

    def move_K_ISBN(self):
        self.InitMoveData('K_ISBN',
                          'ID',
                          'GEN_K_ISBN')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.MoveData_MV()

    def move_TYP_KSIAZKI(self):
        self.InitMoveData('TYP_KSIAZKI',
                          'ID',
                          'GEN_TYP_KSIAZKI')
        self.MoveData_MV()

    def move_K_TYTUL_DOD(self):
        self.InitMoveData('K_TYTUL_DOD',
                          'ID',
                          'GEN_K_TYTUL_DOD')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.MoveData_MV()

    def move_TYP_ODP(self):
        self.InitMoveData('TYP_ODP',
                          'ID',
                          'GEN_TYP_ODP')
        self.MoveData_MV()

    def move_PARAMETRY(self):
        self.InitMoveData('PARAMETRY',
                          '',
                          '')
        self.MoveData_MV()

    def move_UR_USERS(self):
        self.InitMoveData('UR_USERS',
                          'ID_USR',
                          'GEN_UR_USERS')
        self.MoveData_MV()

    def move_K_ODPOW(self):
        self.InitMoveData('K_ODPOW',
                          'ID_ODPOW',
                          'GEN_K_ODPOW')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.MoveData_MV()

    def move_K_OPIS_DET(self):
        self.InitMoveData('K_OPIS_DET',
                          'ID_KATALOG',
                          '')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.MoveData_MV()

    def move_K_WYDAWCA(self):
        self.InitMoveData('K_WYDAWCA',
                          'ID',
                          'GEN_K_WYDAWCA')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.AddFK_MV('K_INSTYT', 'ID_INSTYT')
        self.MoveData_MV()

    def move_K_HASLO(self):
        self.InitMoveData('K_HASLO',
                          'ID_HAS',
                          'GEN_K_HASLO')
        self.MoveData_MV()

    def move_K_HASLO_OPISU(self):
        self.InitMoveData('K_HASLO_OPISU',
                          'ID',
                          'GEN_K_HASLO_OPISU')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.AddFK_MV('K_HASLO', 'ID_HAS')
        self.MoveData_MV()

    def move_K_TYP_UWAGI(self):
        self.InitMoveData('K_TYP_UWAGI',
                          'ID_TYP_UWG',
                          'GEN_K_TYP_UWAGI')
        self.mv_db2.CorrectGenerator(self.mv_table_name)
        self.MoveData_MV()

    def move_K_UWAGA(self):
        self.InitMoveData('K_UWAGA',
                          'ID_UWG',
                          'GEN_K_UWAGA')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.AddFK_MV('K_TYP_UWAGI', 'ID_TYP_UWG')
        self.MoveData_MV()

    def move_K_SERIA(self):
        self.InitMoveData('K_SERIA',
                          'ID_SER',
                          'GEN_K_SERIA')
        self.AddFK_MV('K_INSTYT', 'ID_INSTYT')
        self.MoveData_MV()

    def move_K_SERIA_OPISU(self):
        self.InitMoveData('K_SERIA_OPISU',
                          'ID',
                          'GEN_K_SERIA_OPISU')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.AddFK_MV('K_SERIA', 'ID_SER')
        self.MoveData_MV()

    def move_LEGITYMACJE(self):
        self.InitMoveData('LEGITYMACJE',
                          'ID_LEGITYMACJE',
                          '')
        self.MoveData_MV()

    def move_K_JEZYK_OPISU(self):
        self.InitMoveData('K_JEZYK_OPISU',
                          'ID',
                          'GEN_K_JEZYK_OPISU')
        self.AddFK_MV('K_OPIS', 'ID_KATALOG')
        self.MoveData_MV()

    def move_ZDJECIE(self):
        self.InitMoveData('ZDJECIE',
                          'ID_ZDJ',
                          'GEN_ZDJECIE')
        self.MoveData_MV()

    def move_K_ZAGADN_TYP_DOK(self):
        self.InitMoveData('K_ZAGADN_TYP_DOK',
                          'ID_K_ZAGADN_TYP_DOK',
                          '')
        self.MoveData_MV()

    def move_POWIADOMIENIA(self):
        self.InitMoveData('POWIADOMIENIA',
                          'ID_POWIADOMIENIA',
                          '')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.MoveData_MV()

    def move_ZEST_BIBLIOGRAFICZNE(self):
        self.InitMoveData('ZEST_BIBLIOGRAFICZNE',
                          'ID_ZEST_BIBLIOGRAFICZNE',
                          '')
        self.MoveData_MV()

    def move_K_ZAGADNIENIOWA_NEW(self):
        self.InitMoveData('K_ZAGADNIENIOWA_NEW',
                          'ID_K_ZAGADNIENIOWA_NEW',
                          '')
        self.AddFK_MV('K_ZAGADN_TYP_DOK', 'ID_K_ZAGADN_TYP_DOK')
        self.MoveData_MV()

    def move_ZEST_BIBLIOGRAF_POZYCJE(self):
        self.InitMoveData('ZEST_BIBLIOGRAF_POZYCJE',
                          'ID_ZEST_BIBLIOGRAF_POZYCJE',
                          'ZEST_BIBLIOGRAF_POZYCJE_GEN')
        self.AddFK_MV('ZEST_BIBLIOGRAFICZNE', 'ID_ZEST_BIBLIOGRAFICZNE')
        self.AddFK_MV('K_OPIS', 'ID_K_OPIS')
        self.AddFK_MV('K_ZAGADNIENIOWA_NEW', 'ID_K_ZAGADNIENIOWA_NEW')
        self.MoveData_MV()

    def move_K_ZAGADN_HASLO(self):
        self.InitMoveData('K_ZAGADN_HASLO',
                          'ID_K_ZAGADN_HASLO',
                          'K_ZAGADN_HASLO_GEN')
        self.AddFK_MV('K_HASLO', 'ID_HASLO')
        self.AddFK_MV('K_ZAGADNIENIOWA_NEW', 'ID_K_ZAGADNIENIOWA_NEW')
        self.MoveData_MV()

    def move_CZYTELNIA_ZBIORY(self):
        self.InitMoveData('CZYTELNIA_ZBIORY',
                          '',
                          '')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.AddFK_MV('CZYTELNIA', 'ID_CZYTELNIA')
        self.MoveData_MV()

    def move_UR_PROGRAMS(self):
        self.InitMoveData('UR_PROGRAMS',
                          'ID_PROG',
                          '')
        self.MoveData_MV()

    def move_CZYT_IMIONA(self):
        self.InitMoveData('CZYT_IMIONA',
                          'IMIE',
                          '')
        self.MoveData_MV()

    def move_CZYT_MIASTA(self):
        self.InitMoveData('CZYT_MIASTA',
                          'MIASTO',
                          '')
        self.MoveData_MV()

    def move_MARC_LANG(self):
        self.InitMoveData('MARC_LANG',
                          'ID',
                          '')
        self.MoveData_MV()

    def move_ZAMOWIENIA(self):
        self.InitMoveData('ZAMOWIENIA',
                          'ID_ZAMOWIENIA',
                          '')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.MoveData_MV()

    def move_PROTOKOLY_UBYTKOW(self):
        self.InitMoveData('PROTOKOLY_UBYTKOW',
                          'ID_PROTOKOLY_UBYTKOW',
                          'PROTOKOLY_UBYTKOW_GEN')
        self.AddFK_MV('KSIEGA_INW', 'ID_KS')
        self.MoveData_MV()

    def move_UR_RIGHTS(self):
        self.InitMoveData('UR_RIGHTS',
                          '',
                          '')
        self.AddFK_MV('UR_PROGRAMS', 'ID_PROG')
        self.AddFK_MV('UR_RIGHTS', 'ID_PROG')
        self.AddFK_MV('UR_RIGHTS', 'ID_PARENT_RIGHT')
        self.AddFK_MV('UR_RIGHTS', 'ID_PROG')
        self.AddFK_MV('UR_RIGHTS', 'ID_PARENT_RIGHT')
        self.MoveData_MV()

    def move_UR_JN_USERS_PROGRAMS(self):
        self.InitMoveData('UR_JN_USERS_PROGRAMS',
                          '',
                          '')
        self.AddFK_MV('UR_PROGRAMS', 'ID_PROG')
        self.AddFK_MV('UR_USERS', 'ID_USR')
        self.MoveData_MV()

    def move_REJESTR_UBYTKOW(self):
        self.InitMoveData('REJESTR_UBYTKOW',
                          'ID_REJESTR_UBYTKOW',
                          'REJESTR_UBYTKOW_GEN')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.AddFK_MV('TYP_UBYTKU', 'TYP_UBYTKU')
        self.AddFK_MV('PROTOKOLY_UBYTKOW', 'ID_PROTOKOLY_UBYTKOW')
        self.MoveData_MV()

    def move_MONITY(self):
        self.InitMoveData('MONITY',
                          'ID_MONIT',
                          '')
        self.MoveData_MV()

    def move_MONITY_POZYCJE(self):
        self.InitMoveData('MONITY_POZYCJE',
                          'ID_MONIT_POZ',
                          '')
        self.AddFK_MV('WYPOZYCZENIA', 'ID_WYP')
        self.AddFK_MV('MONITY', 'ID_MONIT')
        self.MoveData_MV()

    def move_MONITY_WYSYLKA(self):
        self.InitMoveData('MONITY_WYSYLKA',
                          'ID_MONIT_WYS',
                          '')
        self.AddFK_MV('MONITY', 'ID_MONIT')
        self.MoveData_MV()

    def move_MONITY_OPISY(self):
        self.InitMoveData('MONITY_OPISY',
                          'ID',
                          '')
        self.MoveData_MV()

    def move_PRACOWNIA(self):
        self.InitMoveData('PRACOWNIA',
                          'ID_PRAC',
                          'GEN_ID_PRACOWNIA')
        self.AddFK_MV('CZYTELNIK', 'ID_CZYTELNIK')
        self.AddFK_MV('JEDN_KLASY', 'ID_KLASA')
        self.MoveData_MV()

    def move_AC_RODZAJE(self):
        self.InitMoveData('AC_RODZAJE',
                          'ID_RODZ',
                          '')
        self.MoveData_MV()

    def move_AC_CZASOPISMA(self):
        self.InitMoveData('AC_CZASOPISMA',
                          'ID_CZAS',
                          'GEN_ID_AC_CZASOPISMA')
        self.AddFK_MV('AC_RODZAJE', 'ID_RODZ')
        self.MoveData_MV()

    def move_AC_NUMERY(self):
        self.InitMoveData('AC_NUMERY',
                          'ID_NUM',
                          'GEN_ID_AC_NUMERY')
        self.AddFK_MV('AC_CZASOPISMA', 'ID_CZAS')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.MoveData_MV()

    def move_CZYTELNIA_AC(self):
        self.InitMoveData('CZYTELNIA_AC',
                          '',
                          '')
        self.AddFK_MV('CZYTELNIA', 'ID_CZYTELNIA')
        self.AddFK_MV('AC_NUMERY', 'ID_NUM')
        self.MoveData_MV()

    def move_KODY_ETYKIETY(self):
        self.InitMoveData('KODY_ETYKIETY',
                          'ID_KODY_ETYKIETY',
                          'GEN_KODY_ETYKIETY')
        self.MoveData_MV()

    def move_REJESTR_SKONTRUM(self):
        self.InitMoveData('REJESTR_SKONTRUM',
                          'ID_REJESTR_SKONTRUM',
                          'REJESTR_SKONTRUM_GEN')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.AddFK_MV('PROTOKOLY_UBYTKOW', 'ID_PROTOKOLY_UBYTKOW')
        self.AddFK_MV('KSIEGA_INW', 'ID_KS')
        self.MoveData_MV()

    def move_AUDIT_HEADER(self):
        self.InitMoveData('AUDIT_HEADER',
                          'ID_AH',
                          'GEN_AUDIT_HEADER')
        self.MoveData_MV()

    def move_AUDIT_DETAIL(self):
        self.InitMoveData('AUDIT_DETAIL',
                          'ID_AD',
                          'GEN_AUDIT_DETAIL')
        self.AddFK_MV('AUDIT_HEADER', 'ID_AH')
        self.MoveData_MV()

    def move_UR_JN_USERS_RIGHTS(self):
        self.InitMoveData('UR_JN_USERS_RIGHTS',
                          '',
                          '')
        self.AddFK_MV('UR_RIGHTS', 'ID_PROG')
        self.AddFK_MV('UR_RIGHTS', 'ID_RIGHT')
        self.AddFK_MV('UR_RIGHTS', 'ID_PROG')
        self.AddFK_MV('UR_RIGHTS', 'ID_RIGHT')
        self.AddFK_MV('UR_USERS', 'ID_USR')
        self.AddFK_MV('UR_PROGRAMS', 'ID_PROG')
        self.MoveData_MV()

    def move_AUDIT_IMAGE(self):
        self.InitMoveData('AUDIT_IMAGE',
                          'ID_AD',
                          'GEN_AUDIT_IMAGE')
        self.AddFK_MV('AUDIT_HEADER', 'ID_AH')
        self.MoveData_MV()

    def move_UR_LOGIN_LOG(self):
        self.InitMoveData('UR_LOGIN_LOG',
                          'ID_LL',
                          'GEN_UR_LOGIN_LOG')
        self.AddFK_MV('UR_PROGRAMS', 'ID_PROG')
        self.AddFK_MV('UR_USERS', 'ID_USR')
        self.MoveData_MV()

    def move_UR_PARAMETR(self):
        self.InitMoveData('UR_PARAMETR',
                          'ID_UP',
                          '')
        self.MoveData_MV()

    def move_UR_HIST_PASS(self):
        self.InitMoveData('UR_HIST_PASS',
                          'ID_PH',
                          'GEN_UR_HIST_PASS')
        self.AddFK_MV('UR_USERS', 'ID_USR')
        self.MoveData_MV()

    def move_K_ZAGADN_NAST_OZN_ODP(self):
        self.InitMoveData('K_ZAGADN_NAST_OZN_ODP',
                          'ID_K_ZAGADN_NAST_OZN_ODP',
                          '')
        self.AddFK_MV('K_ZAGADNIENIOWA_NEW', 'ID_K_ZAGADNIENIOWA_NEW')
        self.MoveData_MV()

    def move_K_ZAGADN_UWAGA(self):
        self.InitMoveData('K_ZAGADN_UWAGA',
                          'ID_K_ZAGADN_UWAGA',
                          '')
        self.AddFK_MV('K_ZAGADNIENIOWA_NEW', 'ID_K_ZAGADNIENIOWA_NEW')
        self.MoveData_MV()

    def move_K_ZAGADN_INW(self):
        self.InitMoveData('K_ZAGADN_INW',
                          'ID_K_ZAGADN_INW',
                          '')
        self.AddFK_MV('K_ZAGADNIENIOWA_NEW', 'ID_K_ZAGADNIENIOWA_NEW')
        self.AddFK_MV('INWENTARZ', 'ID_INW')
        self.MoveData_MV()

