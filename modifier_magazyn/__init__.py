import os
import sys
import time
from shutil import copyfile
from kinterbasdb import connect, DESCRIPTION_NAME
from core import DBModifier

class ModifierMagazyn(DBModifier):
    def copy_from_TOWARY_to_SKLADNIKI(self, src_field_name, dst_field_name = ''):
        self.alert_start('kopiowanie z MAG_TOWARY do STOL_SKLADNIKI [%s]' % src_field_name)
        if dst_field_name == '':
            dst_field_name = src_field_name
        sql = ('update STOL_SKLADNIKI S set S.%s = '+
               '(select T.%s from MAG_TOWARY T where T.ID_TOW = S.ID_TOW)'+
               'where S.%s is null and S.ID_TOW is not null')
        self.execute(sql % (dst_field_name, src_field_name, dst_field_name))
        self.alert_stop()

    def correct_zapotrzebowanie_towary(self):
        self.copy_from_TOWARY_to_SKLADNIKI('SKROT')
        self.copy_from_TOWARY_to_SKLADNIKI('NAZWA')
        self.copy_from_TOWARY_to_SKLADNIKI('JM')
        self.commit()

    def correct_column(self, table_name, field_name, before, after):
        if type(before) is str:
            before = "'%s'" % before
        if type(after) is str:
            after = "'%s'" % after
        self.alert_start('zmiana w %s [%s] z %s na %s' % (table_name, field_name, before, after))
        after = after.decode('utf-8').encode('windows-1250')
        before = before.decode('utf-8').encode('windows-1250')
        sql = 'update %s set %s = %s where %s = %s' % (table_name, field_name, after, field_name, before)
        self.execute(sql)
        self.commit()
        self.alert_stop()

    def corrent_kolejnosc_posilkow(self, id_mag, rok):
        '''
        poprawianie w danym magazynie i roku kolejnosci posilkow
        w raportach na podstawie kolejnosci w slowniku posilkow
        '''
        self.alert_start('poprawianie kolejnosci posilkow')
        sql = ('update STOL_POSILKI P set P.KOLEJNOSC = ' +
               '(select first 1 S.KOLEJNOSC from STOL_STD_POSILKI S ' +
               'where upper(S.NAZWA) = upper(P.NAZWA)) where exists ' +
               '(select S.KOLEJNOSC from STOL_STD_POSILKI S ' +
               'where upper(S.NAZWA) = upper(P.NAZWA)) ' +
               'and P.ID_ZAPOTRZEBOWANIE in (select Z.ID_ZAPOTRZEBOWANIE ' +
               'from STOL_ZAPOTRZEBOWANIE Z where Z.ID_ZAPOTRZEBOWANIE = ' +
               'P.ID_ZAPOTRZEBOWANIE and Z.IDMAG = 14 and Z.ROK = 2014)')
        self.execute(sql)
        sql = 'update STOL_POSILKI P set P.SLOT_POS = P.KOLEJNOSC where P.SLOT_POS <> P.KOLEJNOSC'
        self.execute(sql)
        self.commit()
        sql = ('Select P.ID_ZAPOTRZEBOWANIE, P.KOLEJNOSC, count(P.KOLEJNOSC) ' +
               'from STOL_POSILKI P ' +
               'group by P.ID_ZAPOTRZEBOWANIE, P.KOLEJNOSC ' +
               'having count(P.KOLEJNOSC) > 1')
        error = False
        for database in self.databases:
            database.cur.execute(sql)
            ret = database.cur.fetchall()
            if ret:
                error = True
                break
        if error:
            self.alert_stop_fail()
        else:
            self.alert_stop()

    def corrent_kolejnosc_posilkow_std(self, posilki):
        self.alert_start('poprawianie kolejnosci w slowniku posilkow')
        for i, posilek in enumerate(posilki):
            posilek = posilek.decode('utf-8').encode('windows-1250')
            sql = "update STOL_STD_POSILKI set KOLEJNOSC = %d where NAZWA = '%s'" % (i+1, posilek)
            self.execute(sql)
        self.commit()
        self.alert_stop()