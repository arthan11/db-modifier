import os
import sys
import time
from shutil import copyfile
from kinterbasdb import connect, DESCRIPTION_NAME
from core import DBModifier

class ImporterWypos():
    def move_JEDNOSTKI(self, ids=[]):
        self.InitMoveData('JEDNOSTKI',
                          'ID_JEDN',
                          'NUM_JEDN_GEN',
                          ids)
        self.MoveData_MV()

    def move_ODPOWIEDZIALNI(self):
        self.InitMoveData('ODPOWIEDZIALNI',
                          'ID_ODPOWIEDZIALNI',
                          'ODPOWIEDZIALNI_GEN')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()

    def move_UMIEJSCOWIENIE(self):
        self.InitMoveData('UMIEJSCOWIENIE',
                          'ID_UMIEJSCOWIENIE',
                          'UMIEJSCOWIENIE_GEN')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()

    def move_KONTRAH(self):
        self.InitMoveData('KONTRAH',
                          'ID_KONTRAH',
                          'NUM_KONTRAH')
        self.MoveData_MV()

    def move_SRODKI(self):
        self.InitMoveData('SRODKI',
                          'ID_SRODKI',
                          'SRODKI_GEN')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.AddFK_MV('ODPOWIEDZIALNI', 'ID_ODPOWIEDZIALNI')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE')
        self.AddFK_MV('JEDNOSTKI', 'id_jedn_likwidacji')
        self.AddFK_MV('KONTRAH', 'ID_KONTRAH')
        self.MoveData_MV()

    def move_SRODKI_SZCZEGOLY(self):
        self.InitMoveData('SRODKI_SZCZEGOLY',
                          'ID_SZCZEGOL',
                          'SRODKI_SZCZEGOLY_GEN')
        self.AddFK_MV('SRODKI', 'ID_SRODKI')
        self.AddFK_MV('ZWIEKSZ_WART', 'ID_ZWIEKSZ_WART')
        self.MoveData_MV()
                
    def move_AMORTYZACJA(self):
        self.InitMoveData('AMORTYZACJA',
                          'ID_AMORTYZACJA',
                          'AMORTYZACJA_GEN')
        self.AddFK_MV('SRODKI', 'ID_SRODKI')
        self.MoveData_MV()

    def move_GRUPY_WYPOSAZENIA(self):
        self.InitMoveData('GRUPY_WYPOSAZENIA',
                          'ID_GR_WYPOSAZENIA',
                          'NUM_GRUPY_WYPOSAZENIA')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()
        
    def move_WYPOS_TRANS(self):
        self.InitMoveData('WYPOS_TRANS',
                          'ID_WYPOS_TRANS',
                          'NUM_WYPOS_TRANS_GEN')
        self.AddFK_MV('GRUPY_WYPOSAZENIA', 'ID_GR_WYPOSAZENIA')
        self.AddFK_MV('WYPOS_TRANS', 'ID_WYPOS_ROZCHOD')
        self.AddFK_MV('ODPOWIEDZIALNI', 'ID_ODPOWIEDZIALNI')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE')
        self.AddFK_MV('KONTRAH', 'ID_KONTRAH')
        self.MoveData_MV()

    def move_WYPOS_KOMISJE(self):
        self.InitMoveData('WYPOS_KOMISJE',
                          'ID_WYPOS_KOMISJE',
                          'GEN_WYPOS_KOMISJE')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.MoveData_MV()
    
    def  move_INWENTARYZACJA(self):
        self.InitMoveData('INWENTARYZACJA',
                          'ID_INWENTARYZACJA',
                          'NUM_INWENTARYZACJA_GEN')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN')
        self.AddFK_MV('ODPOWIEDZIALNI', 'ID_ODPOWIEDZIALNI')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE')
        self.AddFK_MV('WYPOS_KOMISJE', 'ID_WYPOS_KOMISJE')
        self.MoveData_MV()

    def move_INWENT_ARKUSZE(self):
        self.InitMoveData('INWENT_ARKUSZE',
                          'ID_INWENT_ARKUSZE',
                          'NUM_INWENT_ARKUSZE_GEN')
        self.AddFK_MV('INWENTARYZACJA', 'ID_INWENTARYZACJA')
        self.AddFK_MV('ODPOWIEDZIALNI', 'ID_ODPOWIEDZIALNI')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE')
        self.AddFK_MV('WYPOS_KOMISJE', 'ID_WYPOS_KOMISJE')
        self.MoveData_MV()
    
    def move_INWENT_LISTA(self):
        self.InitMoveData('INWENT_LISTA',
                          'ID_INWENT_LISTA',
                          'NUM_INWENT_LISTA_GEN')
        self.AddFK_MV('INWENTARYZACJA', 'ID_INWENTARYZACJA')
        self.AddFK_MV('ODPOWIEDZIALNI', 'ID_ODPOWIEDZIALNI')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE')
        self.AddFK_MV('GRUPY_WYPOSAZENIA', 'ID_GRUPY_WYPOSAZENIA')
        self.AddFK_MV('INWENT_ARKUSZE', 'ID_INWENT_ARKUSZE')
        self.AddFK_MV('WYPOS_TRANS', 'ID_WYPOSAZENIA')
        self.AddFK_MV('SRODKI', 'ID_SRODKA')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE_STWIERDZONE')
        self.MoveData_MV()
    
    def move_ZWIEKSZ_WART(self):
        self.InitMoveData('ZWIEKSZ_WART',
                          'ID_ZWIEKSZ_WART',
                          'ZWIEKSZ_WART_GEN')
        self.AddFK_MV('SRODKI', 'ID_SRODKI')
        self.MoveData_MV()

    def move_WYPOS_RAP_HIST_SRD(self):
        self.InitMoveData('WYPOS_RAP_HIST_SRD',
                          'ID_RAPORT',
                          'WYPOS_RAP_HIST_SRD_GEN')
        self.MoveData_MV()      

    def move_WYPOS_RAP_HIST_WYP(self):
        self.InitMoveData('WYPOS_RAP_HIST_WYP',
                          'ID_RAPORT',
                          'WYPOS_RAP_HIST_WYP_GEN')
        self.MoveData_MV()      

    def move_WYPOS_HIST_SRD(self):
        self.InitMoveData('WYPOS_HIST_SRD',
                          'ID_HIST_SRD',
                          'WYPOS_HIST_SRD')
        #self.AddFK_MV('GRUPY', 'ID_GR_OLD')
        self.AddFK_MV('JEDNOSTKI', 'ID_JEDN_OLD')
        self.AddFK_MV('SRODKI', 'ID_SRODKI_OLD')
        self.AddFK_MV('SRODKI', 'ID_SRODKI_NEW')
        self.AddFK_MV('UMIEJSCOWIENIE', 'ID_UMIEJSCOWIENIE_OLD')
        self.AddFK_MV('ODPOWIEDZIALNI', 'ID_ODPOWIEDZIALNI_OLD')
        self.AddFK_MV('WYPOS_HIST_SRD', 'ID_NEXT')
        self.AddFK_MV('SRODKI', 'ID_RAPORT')
        self.MoveData_MV()      

        
    def zlacz_duble(self, table_name, compare_fields, data_from_source=False):
        pk_name = self.mv_db2.tables[table_name].pk_name
        gen_value = self.mv_db2.tables[table_name].gen_value

        compare_sql_parts = []
        select_sql_parts = []
        print_parts = []

        for field_name in compare_fields:
            compare_sql_parts.append('upper(X2.%s) = upper(X1.%s)' % (field_name, field_name))
            select_sql_parts.append('X1.%s' % (field_name))
            print_parts.append(field_name + ': %s')

        print_msg = '- ' + ', '.join(print_parts)
        select_sql = ', ' + ', '.join(select_sql_parts)
        compare_sql = ' and '.join(compare_sql_parts)
        compare_sql += ' and X2.%s <= %d ' % (pk_name, gen_value)

        sql = 'select X1.%s%s, (select first 1 X2.%s from %s X2 where %s) from %s X1 where X1.%s > %s and exists (select X2.%s from %s X2 where %s)' % (pk_name, select_sql, pk_name, table_name, compare_sql, table_name, pk_name, gen_value, pk_name, table_name, compare_sql)

        # print sql
        self.mv_db2.cur.execute(sql)
        data = self.mv_db2.cur.fetchall()

        if data:
            self.alert('\nusuwanie dubli w tabeli %s:' % table_name)
            params = []
            for d in data:
                tab = []
                #for x in d[1:len(compare_fields)+1]:
                #    tab.append(str(x))
                params.append((d[0],))
                self.change_PK(self.mv_db2, table_name, d[0], d[-1])
                #self.alert(' - ' +  ' '.join(tab))
            try:
                sql = 'delete from %s where %s = ?' % (table_name, pk_name)
                self.mv_db2.cur.executemany(sql, tuple(params))
                self.mv_db2.con.commit()
                self.alert_stop()
            except:
                self.alert_stop_fail()

    def change_PK(self, db, table_name, old_id, new_id, in_root=False):
        sql_base = 'update %s set %s = %d where %s = %d'
        for tab in db.tables:
            for fk in db.tables[tab].fk:
                if fk[0] == table_name:
                    if True: #try:
                        self.alert_start('zmiana FK w tabeli %s z %d na %d' % (tab, old_id, new_id))
                        sql = sql_base % (tab,
                                          fk[1],
                                          new_id,
                                          fk[1],
                                          old_id)
                        db.cur.execute(sql)
                        field_idx = db.tables[tab].FieldIdx(fk[1])
                        if not field_idx:
                            self.alert_stop()
                            continue
                        for i, val in enumerate(db.tables[tab].values):
                            l = list(val)
                            if l[field_idx]:
                                l[field_idx] = new_id
                                self.databases[-1].tables[tab].values[i] = tuple(l)
                        self.alert_stop()
                    #except:
                    #    self.alert_stop_fail()

        if in_root:
            try:
                self.alert_start('zmiana PK w tabeli %s z %d na %d' % (table_name, old_id, new_id))
                pk_name = db.tables[table_name].pk_name
                sql = sql_base % (table_name,
                                  pk_name,
                                  new_id,
                                  pk_name,
                                  old_id)
                db.cur.execute(sql)
                self.alert_stop()
            except:
                self.alert_stop_fail()
        db.con.commit()

    def clear_column(self, db, table_name, field_name):
        self.alert_start('usuwanie zawartosci kolumny %s z %s' % (field_name, table_name))
        try:
            sql = 'update %s set %s = null' % (table_name, field_name)
            db.cur.execute(sql)
            db.con.commit()
            self.alert_stop()
        except:
            self.alert_stop_fail()

    def delete(self, db, table_name, row_id):
        self.alert_start('usuwanie wiersza %d z tabeli %s' % (row_id, table_name))
        field_name = db.tables[table_name].pk_name
        try:
            sql = 'delete from %s where %s = %d' % (table_name, field_name, row_id)
            db.cur.execute(sql)
            db.con.commit()
            self.alert_stop()
        except:
            self.alert_stop_fail()