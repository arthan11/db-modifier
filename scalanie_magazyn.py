import os
import time
import sys
from shutil import copyfile
from core import DBModifier, Database, Table
from importer_magazyn import ImporterMagazyn
from db_info import DBInfo

class Importer(DBModifier, ImporterMagazyn):
    pass

    
if __name__ == "__main__":
    folder = os.path.dirname(os.path.abspath(__file__))
    folder = os.path.join(folder, '2017.11', '')
    DB1 = r'FIN1.GDB'
    DB2 = r'FIN2.GDB'
    # reset baz danych
    try:
        if os.path.isfile(os.path.join(folder, DB1)):
            os.remove(os.path.join(folder, DB1))
        if os.path.isfile(os.path.join(folder, DB2)):
            os.remove(os.path.join(folder, DB2))
        # print os.path.join(folder, 'gbk', DB1)
        copyfile(os.path.join(folder, 'db', DB1), os.path.join(folder, DB1))
        copyfile(os.path.join(folder, 'db', DB2), os.path.join(folder, DB2))
    except:
        print 'Nie mozna zresetowac bazy danych!'
        sys.exit()

    start = time.time()
    imp = Importer()
    imp.AddDB(folder+DB1)
    imp.AddDB(folder+DB2)

    imp.mv_db1 = imp.databases[0]
    imp.mv_db2 = imp.databases[1]

    info = DBInfo()
    info.host = '127.0.0.1'
    for dbname in [folder+DB1, folder+DB2]:
        info.database = dbname
        info.LoadDB()
        l = info.get_programs_versions()
        for p in l:
            if p['app'] == 'MAGAZYN':
                print '{} - {} ver.{}'.format(os.path.basename(dbname), p['app'], p['ver'])
                break

    imp.clear_table(imp.mv_db2, 'MAG_OPERACJE', 'IDOPE')
    imp.clear_table(imp.mv_db2, 'MAG_DOKUMENTY', 'IDDOK')
    imp.clear_table(imp.mv_db2, 'MAG_REJESTR_TOWAR_ROK')
    imp.clear_table(imp.mv_db2, 'STOL_GMPGHP_ZAPOT')
    imp.clear_table(imp.mv_db2, 'STOL_GRUPY_OSOB')
    imp.clear_table(imp.mv_db2, 'STOL_POSILKI')
    imp.clear_table(imp.mv_db2, 'STOL_ZAPOTRZEBOWANIE')
    imp.clear_table(imp.mv_db2, 'STOL_SKLADNIKI')
    imp.clear_table(imp.mv_db2, 'MAG_TOWARY')
    #imp.clear_table(imp.mv_db2, 'STOL_NAZWY_WART_ODZYW')

    imp.move_JEDNOSTKI()#[10])
    imp.move_TYPY_SZKOL()
    imp.move_KONTRAH()
    imp.move_MAG_MAGAZYN()
    imp.move_MAG_DOKUMENTY()
    imp.move_MAG_GRUPY_TOW()
    imp.move_MAG_GRUPY_TOW_MAG()
    imp.move_MAG_TOWARY()
    imp.move_MAG_REJESTR_TOWAR_ROK()
    
    imp.move_MAG_OPERACJE()
    imp.move_MAG_INWENT()
    imp.move_MAG_INWENT_POZYCJE()
    imp.move_STOL_ZAPOTRZEBOWANIE()
    imp.move_STOL_GMPGHP()
    imp.move_STOL_GMPGHP_REC()
    imp.move_STOL_GMPGHP_ZAPOT()
    imp.move_STOL_GRUPY_OSOB()
    imp.move_STOL_POSILKI()
    imp.move_STOL_SKLADNIKI()
    imp.move_STOL_STAWKI_POSILKOW()
    imp.move_STOL_WART_ODZYW()
    imp.zlacz_duble('STOL_GMPGHP', ['NAZWA'])
    
    imp.move_MAG_KONTO_KSIEG()
    
    
    imp.zlacz_duble('KONTRAH', ['SKROT', 'NAZWA'])
    
    imp.update(imp.mv_db2, 'MAG_GRUPY_TOW', 3, 'SKROT', 'WARZYWA I OWOCE')
    imp.update(imp.mv_db2, 'MAG_GRUPY_TOW', 3, 'NAZWA', 'WARZYWA I OWOCE')
    
    imp.zlacz_duble('MAG_GRUPY_TOW', ['SKROT', 'NAZWA'])
    
    imp.mv_db2.cur.execute('DELETE FROM MAG_GRUPY_TOW_MAG WHERE IDMAG = 1')
    imp.mv_db2.con.commit()
    
    imp.zlacz_duble('MAG_MAGAZYN', ['SKROT', 'NAZWA'], data_from_source=True)

    imp.update(imp.mv_db2, 'JEDNOSTKI', 12, 'NAZWA', 'SPNR11')
    imp.zlacz_duble('JEDNOSTKI', ['NAZWA'])

    #imp.mv_db2.cur.execute('ALTER TABLE MAG_GRUPY_TOW_MAG DROP CONSTRAINT INTEG_323')
    #imp.mv_db2.con.commit()
    
    #imp.mv_db2.cur.execute('ALTER TABLE MAG_GRUPY_TOW_MAG ADD CONSTRAINT INTEG_323 PRIMARY KEY (IDGRUPY, IDMAG)')
    #imp.mv_db2.con.commit()
        
    #imp.change_PK(imp.mv_db2, 'JEDNOSTKI', 18, 3)
    #imp.delete(imp.mv_db2, 'JEDNOSTKI', 18)

    end = time.time()
    print 'Ukonczono w %.2fs.' % (end - start)