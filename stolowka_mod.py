# -*- coding: utf-8 -*-
import os
import time
import sys
from shutil import copyfile
from core import DBModifier, Database, Table
from modifier_magazyn import ModifierMagazyn

class Modifier(DBModifier, ModifierMagazyn):
    pass
    
if __name__ == "__main__":
    folder = os.path.abspath(__file__)
    folder = os.path.join(os.path.dirname(folder), '')
    DB1 = r'STOLOWKA.GDB'
    print os.path.join(folder, 'stolowka blad', DB1)
    # reset bazy danych
    try:
        if os.path.isfile(os.path.join(folder, DB1)):
            os.remove(os.path.join(folder, DB1))
        copyfile(os.path.join(folder, 'stolowka blad', DB1), os.path.join(folder, DB1))
    except:
        print 'Nie mozna zresetowac bazy danych!'
        sys.exit()

    start = time.time()
    mod = Modifier()
    mod.AddDB(folder+DB1)

    mod.correct_zapotrzebowanie_towary()
    mod.correct_column('STOL_POSILKI', 'NAZWA', 'Caładzienne', 'Całodzienne')
    # mod.corrent_kolejnosc_posilkow_std(['śniadanie', 'obiad', 'całodzienne'])
    mod.corrent_kolejnosc_posilkow(id_mag=14, rok=2014)

    end = time.time()
    print 'Ukonczono w %.2fs.' % (end - start)