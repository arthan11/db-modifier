import os
import sys
import time
from colorama import init, Fore, Back, Style
from shutil import copyfile
from kinterbasdb import connect, DESCRIPTION_NAME

init()

class Table():
    def __init__(self):
        self.name = ''
        self.pk_name = ''
        self.fk = []
        self.gen_name = ''
        self.gen_value = 0
        self.fields = []
        self.values = []
        self.filtered = False

    def GetInsertSQL(self):
        fields = ','.join(self.fields)
        fields_par = ','.join(('?'*len(self.fields)))
        sql = 'insert into %s (%s) values (%s)'  % (self.name, fields, fields_par)
        return sql
        
    def FieldIdx(self, field_name):
        for i, f in enumerate(self.fields):
            if f == field_name:
                return i
        return None
    
class Database():
    def __init__(self):
      self.con = None
      self.cur = None
      self.tables = {}
      self.external_values = False
      
    def LoadDB(self, database):
        self.con = connect(host='127.0.0.1', 
                           database=database,
                           user='sysdba',
                           password='masterkey')
        self.cur = self.con.cursor()

    def GetTable(self, table_name, pk_name, gen_name, ids=[]):
        s_ids = []
        for x in ids:
            s_ids.append(str(x))
        if s_ids:
            where = ' where %s in (%s)' % (pk_name, ','.join(s_ids))

        else:
            where = ''
        if pk_name != '':
            order_by = ' order by %s' % (pk_name)
        else:
            order_by = ''
        self.cur.execute("select * from %s%s%s" % (table_name, where, order_by))
        tab = Table()
        tab.name = table_name
        tab.filtered = (where != '')
        tab.pk_name = pk_name
        tab.gen_name = gen_name
        tab.values = self.cur.fetchall()
        for fieldDesc in self.cur.description:
            tab.fields.append(fieldDesc[DESCRIPTION_NAME])
        
        #generator
        if gen_name != '':
            self.cur.execute('select gen_id(%s, 0) from rdb$database' % gen_name)
            tab.gen_value = self.cur.fetchone()[0]

        self.tables[table_name] = tab
        #print tab
        return tab
        
    def CorrectGenerator(self, table_name, new_value=0):
        gen_name = self.tables[table_name].gen_name
        if self.tables[table_name].pk_name != '':
            self.cur.execute('select max(%s) from %s' % (self.tables[table_name].pk_name, table_name))
            gen_new_value = self.cur.fetchone()[0]
        else:
            gen_new_value = new_value
        if gen_name != '':
            self.cur.execute('ALTER SEQUENCE %s RESTART WITH %d' % (gen_name, gen_new_value))
        else:
            self.tables[table_name].gen_value = gen_new_value



class DBModifier():
    def __init__(self):
        self.databases = []
        self.mv_db1 = None
        self.mv_db2 = None
        self.mv_table_name = ''
        self.mv_pk = ''
        self.mv_gen = ''
        self.external_values = False
    
    def get_external_values(self):
        pass
    
    def AddDB(self, name):
        db = Database()
        db.LoadDB(name)
        self.databases.append(db)

    def GetTable(self, table_name, pk_name, gen_name, ids=[]):
        for database in self.databases:
            # filtrowanie w tabeli zrodlowej
            if database == self.mv_db1:
                database.GetTable(table_name, pk_name, gen_name, ids)
            else:
                database.GetTable(table_name, pk_name, gen_name)
        
    def AddFK(self, table_name, table_name_fk, field_name):
        for database in self.databases:
            database.tables[table_name].fk.append([table_name_fk, field_name])

    def commit(self):
        for database in self.databases:
            database.con.commit()

    def execute(self, sql):
        for database in self.databases:
            database.cur.execute(sql)

    def alert(self, msg, new_line=True):
        enc = sys.stdout.encoding
        if new_line:
            print msg.decode('utf8').encode(enc)
        else:
            print msg,

    def alert_start(self, msg, space=0):
        enc = sys.stdout.encoding
        msg =  msg.decode('utf8').encode(enc)
        self.alert(msg + ' ' + '.'*(71-len(msg)-space), False)

    def alert_stop(self):
        self.alert(Fore.GREEN + '[ OK ]'+ Style.RESET_ALL)

    def alert_stop_fail(self, exit=True):
        self.alert(Fore.RED + '[FAIL]'+ Style.RESET_ALL)
        if exit:
            sys.exit()

    def FilterValues(self, values, field_idx, table, gen_value):
        filtered = False
        new_values = []
        for value in values:
            to_remove = True
            id1 = value[field_idx]
            if not id1:
                to_remove = False
            else:
                id1 = id1 + gen_value
                for value2 in table.values:
                    id2 = value2[0]
                    if id1 == id2:
                        to_remove = False
                        break
            if not to_remove:
                new_values.append(value)
            else:
                filtered = True
        return filtered, new_values


    def MoveData(self, src, dst, table_name):
        src_table = src.tables[table_name]
        dst_table = dst.tables[table_name]
        
        insert_sql = src_table.GetInsertSQL()
        # pobranie danych z tabeli zrodlowej
        values = src_table.values
        # pobranie przesuniecia pk
        if dst_table.gen_name == '':
            dst.CorrectGenerator(table_name)
        pk_delta = dst_table.gen_value
        #print pk_delta.__class__
        if pk_delta.__class__ is str:
            pk_delta = ''
        elif pk_delta.__class__ is unicode:
            pk_delta = u''
        # pobranie indeksu pola pk w tabeli
        if src_table.pk_name != '':
            pk_idx = src_table.FieldIdx(src_table.pk_name)
        
        # utworzenie tabeli z przesunieciami id
        deltas = []
        if src_table.pk_name != '':
            deltas.append([pk_idx, pk_delta])
        for table, field in src_table.fk:
            idx = src_table.FieldIdx(field)
            if idx == None:
                print 'brak pola:', field
                continue
            delta = dst.tables[table].gen_value
            # usuwanie z pamieci danych powiazanych z odfiltrowanymi danymi
            if src.tables[table].filtered:
                filtered_values = self.FilterValues(values, idx, src.tables[table], dst.tables[table].gen_value)
                filtered, values = filtered_values[0], filtered_values[1]
                if filtered:
                    src.tables[table_name].filtered = True
                    src.tables[table_name].values = values

            # przesuniecie id by nie nachodzilo na istniejace
            if idx != None:
                deltas.append([idx, delta])
            
        # przeprowadzenie przesuniec id w tabeli z danymi
        for i, val in enumerate(values):
            l = list(val)
            for idx, delta in deltas:
                if l[idx]:
                    l[idx] = l[idx] + delta
            values[i] = tuple(l)

        # jezeli id jest varchar to nie przenosimy takich samych wierszy
        rows_to_del = []
        if pk_delta.__class__ != int:
            for src_row in values:
                for dst_row in dst.tables[table_name].values:
                    is_row_in_dst = True
                    if src_row[0] == dst_row[0]:
                        rows_to_del.append(src_row)
                        break
        for row in rows_to_del:
            values.remove(row)
        #src.tables[table_name].values = values
            
        # zapisanie danych do bazy
        try:
            dst.cur.executemany(insert_sql, values)
            #print values
        except:
            self.alert_stop_fail(False)
            try:
                for value in values:
                    dst.cur.execute(insert_sql, value)
            except:
                print insert_sql
                print value
                dst.cur.execute(insert_sql, value)
        l = str(len(values))
        print '\b' + '.' * (8-len(l)) + ' ' + l,
        if (src_table.gen_name != '') and (len(values) != 0):
            dst.CorrectGenerator(table_name, src.tables[table_name].gen_value)
        dst.con.commit()
        self.alert_stop()

    def InitMoveData(self, mv_table_name, mv_pk, mv_gen, ids=[]):
        self.mv_table_name = mv_table_name
        self.mv_pk = mv_pk
        self.mv_gen = mv_gen
        self.alert_start('kopiowanie tabeli %s' % mv_table_name, 9)
        self.GetTable(mv_table_name, mv_pk, mv_gen, ids)
        if self.external_values:
            self.get_external_values()
        
    def AddFK_MV(self, table_name_fk, field_name):
        self.AddFK(self.mv_table_name, table_name_fk, field_name)
        
    def MoveData_MV(self):
        self.MoveData(self.mv_db1, self.mv_db2, self.mv_table_name)
        
    def zlacz_duble(self, table_name, compare_fields, data_from_source=False):
        pk_name = self.mv_db2.tables[table_name].pk_name
        gen_value = self.mv_db2.tables[table_name].gen_value

        compare_sql_parts = []
        select_sql_parts = []
        print_parts = []

        for field_name in compare_fields:
            compare_sql_parts.append('upper(X2.%s) = upper(X1.%s)' % (field_name, field_name))
            select_sql_parts.append('X1.%s' % (field_name))
            print_parts.append(field_name + ': %s')

        print_msg = '- ' + ', '.join(print_parts)
        select_sql = ', ' + ', '.join(select_sql_parts)
        compare_sql = ' and '.join(compare_sql_parts)
        compare_sql += ' and X2.%s <= %d ' % (pk_name, gen_value)

        sql = 'select X1.%s%s, (select first 1 X2.%s from %s X2 where %s) from %s X1 where X1.%s > %s and exists (select X2.%s from %s X2 where %s)' % (pk_name, select_sql, pk_name, table_name, compare_sql, table_name, pk_name, gen_value, pk_name, table_name, compare_sql)

        #print sql
        self.mv_db2.cur.execute(sql)
        data = self.mv_db2.cur.fetchall()

        if data:
            self.alert('\nusuwanie dubli w tabeli %s:' % table_name)
            params = []
            for d in data:
                tab = []
                #for x in d[1:len(compare_fields)+1]:
                #    tab.append(str(x))
                if data_from_source:
                    params.append((d[-1],))
                    self.change_PK(self.mv_db2, table_name, d[-1], d[0])
                else:
                    params.append((d[0],))
                    self.change_PK(self.mv_db2, table_name, d[0], d[-1])
                #self.alert(' - ' +  ' '.join(tab))
            try:
                sql = 'delete from %s where %s = ?' % (table_name, pk_name)
                self.mv_db2.cur.executemany(sql, tuple(params))
                self.mv_db2.con.commit()
                self.alert_stop()
            except:
                self.alert_stop_fail()

    def change_PK(self, db, table_name, old_id, new_id, in_root=False):
        sql_base = 'update %s set %s = %d where %s = %d'
        for tab in db.tables:
            for fk in db.tables[tab].fk:
                if fk[0] == table_name:
                    if True: #try:
                        self.alert_start('zmiana FK w tabeli %s z %d na %d' % (tab, old_id, new_id))
                        sql = sql_base % (tab,
                                          fk[1],
                                          new_id,
                                          fk[1],
                                          old_id)
                        db.cur.execute(sql)
                        field_idx = db.tables[tab].FieldIdx(fk[1])
                        if not field_idx:
                            self.alert_stop()
                            continue
                        for i, val in enumerate(db.tables[tab].values):
                            l = list(val)
                            if l[field_idx]:
                                l[field_idx] = new_id
                                self.databases[-1].tables[tab].values[i] = tuple(l)
                        self.alert_stop()
                    #except:
                    #    self.alert_stop_fail()

        if in_root:
            try:
                self.alert_start('zmiana PK w tabeli %s z %d na %d' % (table_name, old_id, new_id))
                pk_name = db.tables[table_name].pk_name
                sql = sql_base % (table_name,
                                  pk_name,
                                  new_id,
                                  pk_name,
                                  old_id)
                db.cur.execute(sql)
                self.alert_stop()
            except:
                self.alert_stop_fail()
        db.con.commit()

    def clear_column(self, db, table_name, field_name):
        self.alert_start('usuwanie zawartosci kolumny %s z %s' % (field_name, table_name))
        try:
            sql = 'update %s set %s = null' % (table_name, field_name)
            db.cur.execute(sql)
            db.con.commit()
            self.alert_stop()
        except:
            self.alert_stop_fail()

    def clear_table(self, db, table_name, pk_name=None):
        self.alert_start('usuwanie zawartosci tabeli %s' % (table_name))
        try:
            if pk_name:
                sql = 'delete from %s order by %s desc' % (table_name, pk_name)
            else:
                sql = 'delete from %s' % (table_name)
            db.cur.execute(sql)
            db.con.commit()
            self.alert_stop()
        except:
            self.alert_stop_fail()

    def delete(self, db, table_name, row_id, pk=None):
        self.alert_start('usuwanie wiersza %d z tabeli %s' % (row_id, table_name))
        field_name = db.tables[table_name].pk_name
        if (field_name == '') and pk:
            field_name = pk
        try:
            sql = 'delete from %s where %s = %d' % (table_name, field_name, row_id)
            db.cur.execute(sql)
            db.con.commit()
            self.alert_stop()
        except:
            self.alert_stop_fail()
        
    def update(self, db, table_name, row_id, field_name, value):
        self.alert_start('aktualizacja wiersza %d kolumny %s z %s' % (row_id, field_name, table_name))
        pk_field_name = db.tables[table_name].pk_name
        if value.__class__.__name__ == 'str':
            value = '\'%s\'' % (value)
        try:
            sql = 'update %s set %s = %s where %s = %d' % (table_name, field_name, value, pk_field_name, row_id)
            db.cur.execute(sql)
            db.con.commit()
            self.alert_stop()
        except:
            self.alert_stop_fail()

        
        
class DBModifierGenerator():
    def __init__(self):
        self.db = None

    def SetDB(self, name):
        self.db = Database()
        self.db.LoadDB(name)
        
    def get_table_list(self):
        sql = (
            'select rdb$relation_name ' +
            'from rdb$relations ' +
            'where rdb$view_blr is null ' +
            'and (rdb$system_flag is null or rdb$system_flag = 0);'
        )
        self.db.cur.execute(sql)
        values = self.db.cur.fetchall()
        ret = []
        for v in values:
            ret.append(v[0].strip())
        return ret
        
    def get_fks(self, table):
        sql = ('''
            SELECT
                detail_index_segments.rdb$field_name AS field_name,
                master_relation_constraints.rdb$relation_name AS reference_table,
                master_index_segments.rdb$field_name AS fk_field

            FROM
                rdb$relation_constraints detail_relation_constraints
                JOIN rdb$index_segments detail_index_segments ON detail_relation_constraints.rdb$index_name = detail_index_segments.rdb$index_name 
                JOIN rdb$ref_constraints ON detail_relation_constraints.rdb$constraint_name = rdb$ref_constraints.rdb$constraint_name -- Master indeksas
                JOIN rdb$relation_constraints master_relation_constraints ON rdb$ref_constraints.rdb$const_name_uq = master_relation_constraints.rdb$constraint_name
                JOIN rdb$index_segments master_index_segments ON master_relation_constraints.rdb$index_name = master_index_segments.rdb$index_name 

            WHERE
                detail_relation_constraints.rdb$constraint_type = 'FOREIGN KEY'
                AND detail_relation_constraints.rdb$relation_name = '%s'
        ''' % (table))
        self.db.cur.execute(sql)
        values = self.db.cur.fetchall()
        ret = []
        for row in values:
            fk = {
                'fk_field':row[0].strip(),
                'table':row[1].strip(),
                'pk_field':row[2].strip()
            }
            ret.append(fk)
        return ret
        
    def get_pk(self, table):
        sql = ('''
                SELECT s.rdb$field_name
                FROM rdb$index_segments AS s
                LEFT JOIN rdb$relation_constraints AS rc ON (rc.rdb$index_name = s.rdb$index_name)
                WHERE rc.rdb$relation_name = '%s'
                AND rc.rdb$constraint_type = 'PRIMARY KEY'
        ''' % (table))
        self.db.cur.execute(sql)
        values = self.db.cur.fetchall()
        ret = ''
        if len(values) == 1:
            ret = values[0][0].strip()
        return ret

    def get_pks(self):
        sql = ('''
                SELECT r.rdb$relation_name, s.rdb$field_name
                FROM rdb$index_segments AS s
                LEFT JOIN rdb$relation_constraints AS rc ON (rc.rdb$index_name = s.rdb$index_name)
                LEFT JOIN rdb$relations AS r ON (r.rdb$relation_name = rc.rdb$relation_name)
                where r.rdb$view_blr is null
                and (r.rdb$system_flag is null or r.rdb$system_flag = 0)
                AND rc.rdb$constraint_type = 'PRIMARY KEY'
                order by r.rdb$relation_name
        ''')
        self.db.cur.execute(sql)
        values = self.db.cur.fetchall()
        ret = []
        for row in values:
            ret.append([row[0].strip(), row[1].strip()])
        return ret

    def get_generators(self):
        sql = ('''
                execute block
                returns (
                    out_name char(31),
                    out_value bigint)
                as
                begin
                    for select rdb$generator_name from rdb$generators where rdb$system_flag is distinct from 1 into out_name do
                    begin
                        execute statement 'select gen_id(' || out_name || ', 0) from rdb$database' into out_value;
                        suspend;
                    end
                end
        ''')
        self.db.cur.execute(sql)
        values = self.db.cur.fetchall()
        ret = []
        for row in values:
            ret.append([row[0].strip(), row[1]])
        return ret
    


    